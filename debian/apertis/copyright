Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: Isaac Z. Schlueter <i@izs.me> (http://blog.izs.me)
 2011-2022, Isaac Z. Schlueter and Contributors
License: ISC

Files: debian/*
Copyright: 2014 Jérémy Lal <kapouer@melix.org>
  2017-2018 Bastien Roucariès <rouca@debian.org>
  2020-2022 Yadd <yadd@debian.org>
License: ISC

Files: .github/*
Copyright: 2012-2022, Isaac Z. Schlueter and Contributors
 2012-2017, Alexander Makarenko <estliberitas@gmail.com>
 2012-2017, Alex Indigo <iam@alexindigo.com>
 2012-2017, Amine Mouafik <amine@mouafik.fr>
 2012-2017, Andrew de Andrade <andrew@deandrade.com.br>
 2012-2017, Balázs Schwarzkopf <schwarzkopfb@icloud.com>
 2012-2017, Matt Sergeant aka baudehlo <helpme+github@gmail.com>
 2012-2017, Benjamin Coe <ben@npmjs.com>
 2012-2017, Ben McCurdy <bpmccurdy@gmail.com>
 2012-2017, Ben Noordhuis <info@bnoordhuis.nl>
 2012-2017, Corey Richardson <kb1pkl@aim.com>
 2012-2017, Damian Beresford <Damian.Beresford@feedhenry.com>
 2012-2017, Jon Ronnenber <jon.ronnenberg@gmail.com>
 2012-2017, Douglas Campos <qmx@qmx.me>
 2012-2017, Duncan Angus Wilkie (mrdnk) <duncanwilkie@hotmail.com>
 2012-2017, Elliott Cable <github@elliottcable.name>
 2012-2017, Evan Lucas <evanlucas@me.com>
 2012-2017, Forrest L Norvell <forrest@npmjs.com>
 2012-2017, F. Scherwi <fscherwi@users.noreply.github.com>
 2012-2017, George Adams <george.adams@uk.ibm.com>
 2012-2017, Gregor Martynux <gregor@martynus.net>
 2012-2017, Ian Langworth ☠ <ian@langworth.com>
 2012-2017, James Halliday <mail@substack.net>
 2012-2017, James Talmage <james@talmage.io>
 2012-2017, Jan Krems <jan.krems@groupon.com>
 2012-2017, Jason Smith (air) <jhs@iriscouch.com>
 2012-2017, Johannes Wuerbach <johannes.wuerbach@googlemail.com>
 2012-2017, John Resig <jeresig@gmail.com>
 2012-2017, Jo Liss <joliss42@gmail.com>
 2012-2017, Jon Ege Ronnenberg <jon.ronnenberg@gmail.com>
 2012-2017, Kayla Graves <kylgrvs@gmail.com>
 2012-2017, Marc Harter <wavded@gmail.com>
 2012-2017, Mark Wubben <mark@novemberborn.net>
 2012-2017, Max Tobias Weber <maxtobiasweber@gmail.com>
 2012-2017, Miroslav Bajtoš <miroslav@strongloop.com>
 2012-2017, Myk Melez <myk@mykzilla.org>
 2012-2017, Odyssea Tsatalos <ogt@tsatalos.com>
 2012-2017, Pedro P. Candel <kusorbox@gmail.com>
 2012-2017, Ranieri Althoff <ranisalt@gmail.com>
 2012-2017, Jake Verbaten (Raynos) <raynos2@gmail.com>
 2012-2017, Roger Meier <roger@bufferoverflow.ch>
 2012-2017, Ryan Graham <r.m.graham@gmail.com>
 2012-2017, Ryan Graham <ryan@strongloop.com>
 2012-2017, Siddharth Mahendraker <siddharth_mahen@me.com>
 2012-2017, Simeon Vincent <svincent@gmail.com>
 2012-2017, Stein Martin Hustad <stein@hustad.com>
 2012-2017, Maarten Spaans <maarten@nouncy.com>
 2012-2017, Thorsten Lorenz <thlorenz@gmx.de>
 2012-2017, Tim Taubert <tim@timtaubert.de>
 2012-2017, Tom MacWright <tom@macwright.org>
 2012-2017, Trent Mick <trentm@gmail.com>
License: ISC

Files: docs/src/* docs/static/* docs/static/opensans/*
Copyright: Isaac Z. Schlueter
 Tanya Brassie
License: Expat

Files: module-async-hook-domain/.github/* module-jackspeak/.github/* module-libtap/.github/* module-tcompare/.github/*
Copyright: Isaac Z. Schlueter and Contributors
License: ISC

Files: debian/rules
Copyright: debian/node-tap/usr/share/doc/node-$$c/copyright;
License: ISC

Files: docs/*
Copyright: Isaac Z. Schlueter and Tanya Brassie.
License: Expat

Files: module-async-hook-domain/*
Copyright: 2019-2022, Isaac Z. Schlueter and Contributors
License: ISC

Files: module-async-hook-domain/package-lock.json
Copyright: no-info-found
License: ISC

Files: module-bind-obj-methods/*
Copyright: Isaac Z. Schlueter and Contributors
License: ISC

Files: module-bind-obj-methods/package-lock.json
Copyright: no-info-found
License: ISC

Files: module-caller-callsite/*
Copyright: no-info-found
License: Expat

Files: module-caller-callsite/license
Copyright: Sindre Sorhus <sindresorhus@gmail.com> (https://sindresorhus.com)
License: Expat

Files: module-caller-path/*
Copyright: no-info-found
License: Expat

Files: module-caller-path/license
Copyright: Sindre Sorhus <sindresorhus@gmail.com> (https://sindresorhus.com)
License: Expat

Files: module-findit/*
Copyright: no-info-found
License: Expat

Files: module-fs-exists-cached/*
Copyright: no-info-found
License: ISC

Files: module-function-loop/*
Copyright: no-info-found
License: ISC

Files: module-isaacs-import-jsx/*
Copyright: no-info-found
License: Expat

Files: module-isaacs-import-jsx/license
Copyright: Vadim Demedes <vdemedes@gmail.com> (github.com/vadimdemedes)
License: Expat

Files: module-jackspeak/*
Copyright: 2019-2022, Isaac Z. Schlueter and Contributors
License: ISC

Files: module-jackspeak/package-lock.json
Copyright: no-info-found
License: ISC

Files: module-libtap/*
Copyright: 2011-2022, Isaac Z. Schlueter and Contributors
License: ISC

Files: module-libtap/package-lock.json
Copyright: no-info-found
License: ISC

Files: module-own-or-env/*
Copyright: Isaac Z. Schlueter and Contributors
License: ISC

Files: module-own-or/*
Copyright: Isaac Z. Schlueter and Contributors
License: ISC

Files: module-tcompare/*
Copyright: 2019-2022, Isaac Z. Schlueter and Contributors
License: ISC

Files: module-tcompare/package-lock.json
Copyright: no-info-found
License: ISC

Files: module-trivial-deferred/*
Copyright: Isaac Z. Schlueter and Contributors
License: ISC

Files: package-lock.json
Copyright: no-info-found
License: ISC

Files: types-tap/*
Copyright: Microsoft Corporation.
License: Expat

Files: docs/src/images/batteries.gif docs/src/images/brain-3.gif docs/src/images/brain.gif docs/src/images/exclamation.gif docs/src/images/logo.gif docs/src/images/question-mark-2.gif docs/src/images/question-mark.gif docs/static/iosevka/ttf/iosevka-ss09-extendedextralightitalic.ttf docs/static/iosevka/ttf/iosevka-ss09-extendedheavyoblique.ttf docs/static/iosevka/ttf/iosevka-ss09-extendedsemiboldoblique.ttf docs/static/iosevka/ttf/iosevka-ss09-extrabolditalic.ttf docs/static/iosevka/woff2/iosevka-ss09-bold.woff2 docs/static/iosevka/woff2/iosevka-ss09-bolditalic.woff2 docs/static/iosevka/woff2/iosevka-ss09-boldoblique.woff2 docs/static/iosevka/woff2/iosevka-ss09-extended.woff2 docs/static/iosevka/woff2/iosevka-ss09-extendedbold.woff2 docs/static/iosevka/woff2/iosevka-ss09-extendedbolditalic.woff2 docs/static/iosevka/woff2/iosevka-ss09-extendedboldoblique.woff2 docs/static/iosevka/woff2/iosevka-ss09-extendedextrabold.woff2 docs/static/iosevka/woff2/iosevka-ss09-extendedextrabolditalic.woff2 docs/static/iosevka/woff2/iosevka-ss09-extendedextraboldoblique.woff2 docs/static/iosevka/woff2/iosevka-ss09-extendedextralight.woff2 docs/static/iosevka/woff2/iosevka-ss09-extendedextralightitalic.woff2 docs/static/iosevka/woff2/iosevka-ss09-extendedextralightoblique.woff2 docs/static/iosevka/woff2/iosevka-ss09-extendedheavy.woff2 docs/static/iosevka/woff2/iosevka-ss09-extendedheavyitalic.woff2 docs/static/iosevka/woff2/iosevka-ss09-extendedheavyoblique.woff2 docs/static/iosevka/woff2/iosevka-ss09-extendeditalic.woff2 docs/static/iosevka/woff2/iosevka-ss09-extendedlight.woff2 docs/static/iosevka/woff2/iosevka-ss09-extendedlightitalic.woff2 docs/static/iosevka/woff2/iosevka-ss09-extendedlightoblique.woff2 docs/static/iosevka/woff2/iosevka-ss09-extendedmedium.woff2 docs/static/iosevka/woff2/iosevka-ss09-extendedmediumitalic.woff2 docs/static/iosevka/woff2/iosevka-ss09-extendedmediumoblique.woff2 docs/static/iosevka/woff2/iosevka-ss09-extendedoblique.woff2 docs/static/iosevka/woff2/iosevka-ss09-extendedsemibold.woff2 docs/static/iosevka/woff2/iosevka-ss09-extendedsemibolditalic.woff2 docs/static/iosevka/woff2/iosevka-ss09-extendedsemiboldoblique.woff2 docs/static/iosevka/woff2/iosevka-ss09-extendedthin.woff2 docs/static/iosevka/woff2/iosevka-ss09-extendedthinoblique.woff2 docs/static/iosevka/woff2/iosevka-ss09-extrabold.woff2 docs/static/iosevka/woff2/iosevka-ss09-extrabolditalic.woff2 docs/static/iosevka/woff2/iosevka-ss09-extraboldoblique.woff2 docs/static/iosevka/woff2/iosevka-ss09-extralight.woff2 docs/static/iosevka/woff2/iosevka-ss09-extralightitalic.woff2 docs/static/iosevka/woff2/iosevka-ss09-extralightoblique.woff2 docs/static/iosevka/woff2/iosevka-ss09-heavy.woff2 docs/static/iosevka/woff2/iosevka-ss09-heavyitalic.woff2 docs/static/iosevka/woff2/iosevka-ss09-heavyoblique.woff2 docs/static/iosevka/woff2/iosevka-ss09-italic.woff2 docs/static/iosevka/woff2/iosevka-ss09-light.woff2 docs/static/iosevka/woff2/iosevka-ss09-lightitalic.woff2 docs/static/iosevka/woff2/iosevka-ss09-lightoblique.woff2 docs/static/iosevka/woff2/iosevka-ss09-medium.woff2 docs/static/iosevka/woff2/iosevka-ss09-mediumitalic.woff2 docs/static/iosevka/woff2/iosevka-ss09-mediumoblique.woff2 docs/static/iosevka/woff2/iosevka-ss09-oblique.woff2 docs/static/iosevka/woff2/iosevka-ss09-regular.woff2 docs/static/iosevka/woff2/iosevka-ss09-semibold.woff2 docs/static/iosevka/woff2/iosevka-ss09-semibolditalic.woff2 docs/static/iosevka/woff2/iosevka-ss09-semiboldoblique.woff2 docs/static/iosevka/woff2/iosevka-ss09-thin.woff2 docs/static/iosevka/woff2/iosevka-ss09-thinitalic.woff2 docs/static/iosevka/woff2/iosevka-ss09-thinoblique.woff2 docs/static/opensans/OpenSans-Bold.woff docs/static/opensans/OpenSans-BoldItalic.woff docs/static/opensans/OpenSans-CondBold.woff docs/static/opensans/OpenSans-ExtraBold.woff docs/static/opensans/OpenSans-ExtraBoldItalic.woff docs/static/opensans/OpenSans-Italic.woff docs/static/opensans/OpenSans-Light-webfont.woff docs/static/opensans/OpenSans-Light.woff docs/static/opensans/OpenSans-LightItalic.woff
Copyright: Isaac Z. Schlueter
 Tanya Brassie
License: Expat
