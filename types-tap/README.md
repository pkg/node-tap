# Installation
> `npm install --save @types/tap`

# Summary
This package contains type definitions for tap (https://github.com/tapjs/node-tap).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/tap.

### Additional Details
 * Last updated: Sun, 24 Apr 2022 17:31:36 GMT
 * Dependencies: [@types/node](https://npmjs.com/package/@types/node)
 * Global values: none

# Credits
These definitions were written by [zkldi](https://github.com/zkldi).
